<%-- 
    Document   : registration
    Created on : Jun 4, 2018, 2:43:16 AM
    Author     : GUNEL
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        <style>
            .width-200{
                width: 200px !important;   
                margin: 6px;
            }
        </style>
    </head>
    <body>
        <div>
            <center>
                <h1>Registration form</h1>
                <form class="form-horizontal" method="POST" action="register">
                    <table>
                        <tr>
                            <td>
                                <label for="name">Name :</label>
                            </td>
                            <td>
                                <input type="text" class="form-control width-200" id="name" name="name" >
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="surname">Surname :</label>
                            </td>
                            <td>
                                <input type="text" class="form-control width-200" id="surname" name="surname">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="username">Username :</label>
                            </td>
                            <td>
                                <input type="text" class="form-control width-200" id="username" name="username">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="password">Password :</label>
                            </td>
                            <td>
                                <input type="password" class="form-control width-200" id="password" name="password">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="age">Age :</label>
                            </td>
                            <td>
                               <!-- <select class="form-control width-200"  name="age" id="age">
                                    <option label="12" value="12" />
                                    <option label="14" value="14" />
                                    <option label="16" value="16" />
                                </select> -->
                                <input class="form-control width-200" name="age" id="age" type="text">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label >Select a gender :</label>
                            </td>
                            <td style="text-align: center">
                                <div class="radio-inline">
                                    <label>
                                        <input type="radio"  name="gender" value="MALE">
                                        Male
                                    </label>
                                </div>
                                <div class="radio-inline">
                                    <label>
                                        <input type="radio"  name="gender" value="FEMALE">
                                        Female
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="country">Country :</label>
                            </td>
                            <td>
                                <select class="form-control width-200" name="country" id="country">

                                    <option value="Norway" name="norway">Norway</option>
                                    <option value="Australia" name="australia" > Australia</option>
                                    <option value="Switzerland" name="switzerland" > Switzerland</option>
                                    <option value="Germany" name="germany" >Germany</option>
                                    <option value="Singapore" name="singapore" >Singapore</option>
                                    <option value="Netherlands" name="netherlands" >Netherlands</option>
                                    <option value="Ireland" name="ireland" >Ireland</option>
                                    <option value="Canada" name="canada" > Canada</option>
                                    <option value="United States" name="unitedState" > United States</option>
                                    <option value="Hong Kong" name="hongKong" > Hong Kong</option>
                                    <option value="New Zealand" name="newZealand">New Zealand</option>
                                    <option value="Japan" name="japan"> Japan </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="profession">Profession :</label>
                            </td>
                            <td>

                                <select class="form-control width-200" name="profession" id="profession">
                                    <option value="Software developer">Software developer </option>
                                    <option value="Hardware Developer">Hardware Developer</option>
                                    <option value="Project Manager">Project Manager</option>
                                    <option value="Team Leader" >Team Leader</option>
                                    <option value="Business Analyst" >Business Analyst </option>
                                    <option value="System Administrator" >System Administrator</option>
                                    <option value="Network Specialist" >Network Specialist</option>
                                    <option value="Full Stack Developer">Full Stack Developer</option>
                                </select>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" >
                                <input style="float: right" class="btn btn-success width-200" type="submit" value="Register" />
                            </td>
                        </tr>
                    </table>
                    <code style="color:red">${registeredUser}</code>
                </form>


            </center>
        </div>
    </body>
</html>
