<%-- 
    Document   : contact
    Created on : Sep 1, 2018, 3:52:19 AM
    Author     : GUNEL
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

        <link rel="stylesheet" href="secured/css/contact.css"/>

    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="head-logo navbar-brand" href="#">Quora</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/Quora/secured/home.jsp">Home</a></li>
                        <li><a href="/Quora/information.jsp" target="_blank">About</a></li>
                        <li><a href="/Quora/profil.jsp">My Profile</a></li>
                        <li><a href="#" target="_blank">Contact</a></li>
                        <li>
                            <a href="/Quora/profil.jsp">
                                Welcome ${loginedUser.username}
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/Quora/login"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container text-center">
            <h1>Contact us</h1>
            <hr>
            <div class="row">
                <div  class=" c1 col-sm-2 ">
                    
                </div>
                <form action="contact" method="POST">
                    <div class=" c2 col-sm-8 text-center">
                        <div class="row">
                            <div class="alert alert-info alert-dismissable" style="width: 100%;text-align: left;">
                                <a class="panel-close close" data-dismiss="alert">×</a> 
                                <i class="fa fa-coffee"></i>
                                <strong>${alertForContact}</strong>
                            </div>
                        </div>

                        <div class="row">
                            <div class="forContact col-sm-6">
                                <div class="content">

                                    <input class="form-control" type="text" name="name" placeholder="First Name">
                                </div>
                            </div>
                            <div align="center" class=" forContact col-sm-6">
                                <div class="content">

                                    <input class="form-control" type="text" name="surname" placeholder="Last Name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="forContact col-sm-6">
                                <div class="content">

                                    <input class="form-control" type="email"  
                                           name="email" placeholder="E-mail">
                                </div>
                            </div>
                            <div class="forContact col-sm-6">
                                <div class="content">

                                    <input class="form-control" type="tel"
                                           pattern="^\d{3}-\d{3}-\d{2}-\d{2}$"
                                           name="phone" placeholder="Phone Number">
                                    <label style="color: grey;">XXX-XXX-XX-XX</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="forMsg col-sm-6">
                                <div class="content">

                                    <textarea class="txtMsg form-control" name="message" placeholder="Message"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="forSend col-sm-6">
                                <div class="content"  >

                                    <input class="btnSend btn btn-info" type="submit" value="Send" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class=" c1 col-sm-2">
                    
                </div>
            </div>
        </div>
        <br>
        <br>
        <footer class="container-fluid">
            <p>Footer Text</p>
        </footer>
    </body>
</html>
