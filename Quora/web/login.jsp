<%-- 
    Document   : login
    Created on : Jun 1, 2018, 12:16:11 PM
    Author     : GUNEL
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 

        <title>JSP Page</title>
        <style>
            .width-200{
                width:200px !important;
                margin:5px;
            }
        </style>
    </head>
    <body>
    <center>
        <form action="login" method="POST" class="form-horizontal">
            <div class="container-fluid">
                <h2>Login</h2>
                <input type="text" class="form-control width-200"  name="username" placeholder="Enter username"/>
                <input type="password" class="form-control width-200" name="password" placeholder="Enter password"/>
                <input type="submit" class=" btn btn-info width-200" value="Submit"/>
                <p style="color:red">${errorMessage}</p>
                <a href="registration.jsp">
                    Register
                </a>

            </div>   
        </form>

    </center>    
</body>
</html>
