<%-- 
    Document   : home
    Created on : Jun 3, 2018, 4:21:13 AM
    Author     : GUNEL
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" href="css/home.css"/>
        <script src="scripts/home.js" ></script>

    </head>
    <body>

        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class=" head-logo navbar-brand" href="#">Quora</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/Quora/secured/home.jsp">Home</a></li>
                        <li><a href="/Quora/information.jsp" target="_blank">About</a></li>
                        <li><a href="/Quora/profil.jsp" target="_blank">My Profile</a></li>
                        <li><a href="/Quora/contact.jsp" target="_blank">Contact</a></li>
                        <li>
                            <a href="/Quora/profil.jsp" target="_blank">
                                Welcome ${loginedUser.username}
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/Quora/login"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="jumbotron">
            <div class="container text-center">
                <div class="create-question-pnl">
                    <form action="/Quora/post" method="POST">
                        <textarea name="content" rows="4" cols="50" ></textarea>
                        <div><input type="submit" class="btn-post btn btn-info" value="Post"/></div>
                    </form>
                </div> 
                <div class="questions-pnl">
                    <center>
                        <c:forEach var="post" items="${posts}">
                            <div class="question-pnl">
                                <div class="que-header">
                                    <u><strong><a href="#">${post.author.username}</a> posted ${post.created}</strong></u> 
                                    <form method="POST" action="/Quora/vote">
                                        <span class="vote-btn" title="I like this">
                                            <c:choose>
                                                <c:when test="${post.liked==true}">
                                                    <i class="fas fa-star"></i>
                                                </c:when>    
                                                <c:otherwise>
                                                    <i class="far fa-star"></i>
                                                </c:otherwise>
                                            </c:choose>
                                            &nbsp;${post.vote}
                                        </span>
                                        <input style="display: none;" name="postid" value="${post.id}"/>

                                    </form>
                                </div> 
                                <div class="que-content">
                                    ${post.content}
                                </div> 
                                <div class="que-comments">
                                    <i class="fas fa-comments comment-btn"></i>
                                    <br/>
                                    <br/>
                                    <c:forEach var="reply" items="${post.replies}">
                                        <p>
                                            <strong><a href="#">${reply.author.username}</a> commented in ${reply.created}</strong>
                                        <p class="comments-content">${reply.content}</p>
                                        </p> 
                                    </c:forEach>
                                    <a href="/Quora/page/reply?postId=${post.id}">Load more...</a>
                                    <br/>
                                    <form action="/Quora/reply" method="POST">
                                        <textarea 
                                            name="reply"
                                            class="commentArea" 
                                            style="display: none;" 
                                            rows="1" cols="50"
                                            placeholder="Typing anything">
                                        </textarea>
                                        <input style="display: none;" value="${post.id}" name="targetPost" />
                                        <button class="commenterBtn" style="display: none;" type="submit"></button>
                                    </form>
                                </div> 
                            </div> 
                        </c:forEach>
                        <code style="margin: 50px;"><a href="/Quora/page/post">Load more</a></code>


                    </center>
                </div> 
            </div>
        </div>
        <footer class="container-fluid text-center">
            <p>Footer Text</p>
        </footer>

    </body>
</html>
