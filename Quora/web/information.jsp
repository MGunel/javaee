<%-- 
    Document   : information
    Created on : Aug 31, 2018, 7:40:18 AM
    Author     : GUNEL
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="secured/css/info.css"/>


    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="head-logo navbar-brand" href="#">Quora</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/Quora/secured/home.jsp">Home</a></li>
                        <li><a href="/Quora/information.jsp">About</a></li>
                        <li><a href="/Quora/profil.jsp" target="_blank">My Profile</a></li>
                        <li><a href="/Quora/contact.jsp" target="_blank">Contact</a></li>
                        <li>
                            <a href="/Quora/profil.jsp" target="_blank">
                                Welcome ${loginedUser.username}
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/Quora/login"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row content">
                <div class="col-sm-4 sidenav">
                    <img src="secured/image/imgAbout.jpg" class="imgAbout" alt="Photos for Info" >

                   

                </div>

                <div class="col-sm-8">
                    <h4><small>ABOUT APPLICATION</small></h4>
                    <hr>
                    <h2 style="font-family:verdana;">QUORA</h2>
                    <h5><span class="glyphicon glyphicon-time"></span> Created by Gunel Mammadli, JUN 27, 2018.</h5>
                    <br>
                    <p class="infoText">
                        This program is coded by Software developer Mammadli Gunel.
                        It consist of posting person's opinion,questions etc.Other people
                        maybe write comment to post that it is posted.But first of all must
                        become registered.If People have a account then he/she can enter easily
                        to system.And Can post.He/She can edit the personal information,can 
                        change profil photo.Person ,that entered to system, can become logout
                        from system.And then Someone that is enteredd  can like  or can dislike
                        posts only once.All of users share
                        her/his complains and suggestions to us by enter MENU >> CONTACT.
                    </p>
                    <br><br>
                    <hr><hr>
                    <br><br>
                </div>
            </div>
        </div>
        <footer class="container-fluid">
            <p>Footer Text</p>
        </footer>
    </body>
</html>
