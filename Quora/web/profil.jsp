<%-- 
    Document   : profil
    Created on : Jul 11, 2018, 4:45:42 AM
    Author     : GUNEL
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> Welcome ${loginedUser.username}</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" href="secured/css/profil.css"/>
        <script src="secured/scripts/profil.js" ></script>

    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="head-logo navbar-brand" href="#">Quora</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="/Quora/secured/home.jsp">Home</a></li>
                        <li><a href="/Quora/information.jsp" target="_blank">About</a></li>
                        <li><a href="/Quora/profil.jsp">My Profile</a></li>
                        <li><a href="/Quora/contact.jsp" target="_blank">Contact</a></li>
                        <li>
                            <a href="/Quora/profil.jsp">
                                Welcome ${loginedUser.username}
                            </a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/Quora/login"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <h1>Edit Profile</h1>
            <hr>
            <form class="form-horizontal" role="form" method="POST" action="edit" enctype="multipart/form-data">
                <div class="row">
                    <!-- left column -->

                    <div class="col-md-3">
                        <div class="text-center">
                            <!--  <img src="secured/image/100.png" id="preview" class="avatar img-circle" alt="avatar">-->
                            <img src="/Quora/edit" id="preview" class="avatar img-circle" alt="avatar">
                            <h6>Upload a different photo...</h6>

                            <input type="file" name="file" accept="image/*" class="form-control">
                        </div>
                    </div>


                    <!-- edit form column -->
                    <div class="col-md-9 personal-info">

                        <div class="alert alert-info alert-dismissable">
                            <a class="panel-close close" data-dismiss="alert">×</a> 
                            <i class="fa fa-coffee"></i>
                            <strong>${alertMsg}</strong>
                        </div>
                        <h3>Personal info</h3>

                        <!--<form class="form-horizontal" role="form" method="POST" action="edit">-->
                        <div class="form-group">
                            <label class="col-lg-3 control-label">First name:</label>
                            <div class="col-lg-8">
                                <input class="form-control" name="name" type="text" value="${loginedUser.name}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Last name:</label>
                            <div class="col-lg-8">
                                <input class="form-control" name="surname" type="text" value="${loginedUser.surname}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Age:</label>
                            <div class="col-lg-8">
                                <input class="form-control" name="age" type="text" value="${loginedUser.age}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Country:</label>
                            <div class="col-lg-8">
                                <div class="ui-select">
                                    <select name="user_country" class="form-control">
                                        <option value="Norway" name="norway" ${loginedUser.country eq "Norway"?'selected="selected"':''}>
                                            Norway
                                        </option>
                                        <option value="Australia" name="australia" ${loginedUser.country eq "Australia"?'selected="selected"':''}>
                                            Australia
                                        </option>
                                        <option value="Switzerland" name="switzerland" ${loginedUser.country eq "Switzerland"?'selected="selected"':''}>
                                            Switzerland
                                        </option>
                                        <option value="Germany" name="germany" ${loginedUser.country eq "Germany"?'selected="selected"':''}>
                                            Germany
                                        </option>
                                        <option value="Singapore" name="singapore" ${loginedUser.country eq "Singapore"?'selected="selected"':''}>
                                            Singapore
                                        </option>
                                        <option value="Netherlands" name="netherlands" ${loginedUser.country eq "Netherlands"?'selected="selected"':''}>
                                            Netherlands
                                        </option>
                                        <option value="Ireland" name="ireland" ${loginedUser.country eq "Ireland"?'selected="selected"':''}>
                                            Ireland
                                        </option>
                                        <option value="Canada" name="canada" ${loginedUser.country eq "Canada"?'selected="selected"':''}>
                                            Canada
                                        </option>
                                        <option value="United States" name="unitedState" ${loginedUser.country eq "United States"?'selected="selected"':''}>
                                            United States
                                        </option>
                                        <option value="Hong Kong" name="hongKong" ${loginedUser.country eq "Hong Kong"?'selected="selected"':''}>
                                            Hong Kong
                                        </option>
                                        <option value="New Zealand" name="newZealand" ${loginedUser.country eq "New Zealand"?'selected="selected"':''}>
                                            New Zealand
                                        </option>
                                        <option value="Japan" name="japan" ${loginedUser.country eq "Japan"?'selected="selected"':''}>
                                            Japan
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Profession:</label>
                            <div class="col-lg-8">
                                <div class="ui-select">
                                    <select name="user_profession" class="form-control">
                                        <option value="Software developer" ${loginedUser.profession eq "Software developer"?'selected="selected"':''}>
                                            Software developer
                                        </option>
                                        <option value="Hardware Developer" ${loginedUser.profession eq "Hardware Developer"?'selected="selected"':''}>
                                            Hardware Developer
                                        </option>
                                        <option value="Project Manager" ${loginedUser.profession eq "Project Manager"?'selected="selected"':''}>
                                            Project Manager
                                        </option>
                                        <option value="Team Leader" ${loginedUser.profession eq "Team Leader"?'selected="selected"':''}>
                                            Team Leader
                                        </option>
                                        <option value="Business Analyst" ${loginedUser.profession eq "Business Analyst"?'selected="selected"':''}>
                                            Business Analyst 
                                        </option>
                                        <option value="System Administrator" ${loginedUser.profession eq "System Administrator"?'selected="selected"':''}>
                                            System Administrator
                                        </option>
                                        <option value="Network Specialist" ${loginedUser.profession eq "Network Specialist"?'selected="selected"':''}>
                                            Network Specialist
                                        </option>
                                        <option value="Full Stack Developer" ${loginedUser.profession eq "Full Stack Developer"?'selected="selected"':''}>
                                            Full Stack Developer
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Username:</label>
                            <div class="col-md-8">
                                <input class="form-control" name="username" type="text" value="${loginedUser.username}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Password:</label>
                            <div class="col-md-8">
                                <input class="form-control" name="password" type="password" value="${loginedUser.password}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Confirm password:</label>
                            <div class="col-md-8">
                                <input class="form-control" name="confirmpassword" type="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Gender:</label>
                            <div class="col-md-8 radio">
                                <label class="radio-inline">
                                    <input type="radio" value="MALE" name="user[gender]" id="user_gender_male" ${loginedUser.gender eq "MALE"?'checked="checked"':''}>Male
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" value="FEMALE" name="user[gender]" id="user_gender_female" ${loginedUser.gender eq "FEMALE"?'checked="checked"':''}>Female
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-8">
                                <input type="submit" class="btn btn-primary" value="Save Changes">
                                <span></span>
                                <input type="reset" class="btn btn-default" value="Cancel">
                            </div>
                        </div>
                    </div>
                </div>
            </form>



        </div>


        <footer class="container-fluid text-center">
            <p>Footer Text</p>
        </footer>
    </body>
</html>
