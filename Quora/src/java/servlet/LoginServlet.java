package servlet;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pojos.PojoImage;
import pojos.Post;
import pojos.User;
import pojos.Vote;
import repository.ImageRepository;
import repository.PostRepository;
import repository.UserRepository;
import repository.VoteRepository;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author GUNEL
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    // DataManager dataManager;
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final VoteRepository voteRepository;
    private final ImageRepository imageRepository;

    {
        userRepository = new UserRepository();
        postRepository = new PostRepository();
        voteRepository = new VoteRepository();
        imageRepository=new ImageRepository();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("Post Working");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        HttpSession session = req.getSession(true);
        User user = null;
        try {

            user = userRepository.findByUsernameAndPassword(username, password);
            if (user == null) {
                throw new SecurityException("Wrong username and passowrd");
            }
            PojoImage pojoImage=imageRepository.findByUser_id(user);
            user.setPojoImage(pojoImage);
            session.setAttribute("loginedUser", user);
            session.setAttribute("postCurrentPage", 1);
            //session.setAttribute("replyCurrentPage", 1);
            HashMap<Long, Integer> mymapp = new HashMap<>();
            mymapp.put(1L, 1);
            session.setAttribute("replyCurrentPage", mymapp);

            /////////////////Get All POSTS///////////
            //List<Post> allPost = postRepository.findAll();
            List<Post> allPost = postRepository.findAllPaginated(5, 1);
            final long userId = user.getId();
            for (Post p : allPost) {
                List<Vote> votes = voteRepository.findVotesByPost(p.getId());
                p.setLiked(votes.stream().filter(vote -> vote.getUser().getId().equals(userId)).findAny().isPresent());

            }
            Comparator<Post> orderByCreated = new Comparator<Post>() {
                @Override
                public int compare(Post post1, Post post2) {
                    return post2.getCreated().compareTo(post1.getCreated());
                }
            };
            Collections.sort(allPost, orderByCreated);

            session.setAttribute("posts", allPost);

            ///////////////////////////////////////////
            resp.sendRedirect("secured/home.jsp");
        } catch (IOException | SecurityException e) {
            e.printStackTrace(System.err);
            session.removeAttribute("loginedUser");
            resp.sendRedirect("login.jsp");
            session.setAttribute("errorMessage", "Username and Password wrong");

        }

    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (session != null) {
            session.invalidate();
        }
        resp.sendRedirect("login.jsp");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Get Working");
        HttpSession session = req.getSession();
        if (session != null) {
            session.invalidate();
        }
        resp.sendRedirect("login.jsp");
    }

}
