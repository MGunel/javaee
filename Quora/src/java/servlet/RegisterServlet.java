/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pojos.Gender;
import pojos.PojoImage;
import pojos.User;
import repository.ImageRepository;
import repository.UserRepository;

/**
 *
 * @author GUNEL
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

    
    private final UserRepository userRepository;
    private final ImageRepository imageRepository;

    {
        userRepository = new UserRepository();
        imageRepository = new ImageRepository();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(true);
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String age = req.getParameter("age");
        String gender = req.getParameter("gender");
        String country = req.getParameter("country");
        String profession = req.getParameter("profession");
        System.out.println(name + " " + surname + "" + username + password + age + gender + country + profession);
        User user = new User(username,
                password,
                name,
                surname,
                country,
                Gender.valueOf(gender),
                Integer.parseInt(age),
                profession,
                Timestamp.from(Instant.now()));
        try {
            //inserts
            User registeredUser = userRepository.save(user);
            PojoImage pojoImage = new PojoImage();
            pojoImage.setUserId(registeredUser.getId());
            pojoImage.setImgName("defaultUser");
            pojoImage.setImgPath("C:\\Users\\user\\Desktop\\resources\\defaultUser.png");
            user.setPojoImage(pojoImage);
            imageRepository.save(user.getPojoImage());//image cedveli sonradan eleve olundugu ucun
            resp.sendRedirect("login.jsp");
        } catch (IOException e) {
            e.printStackTrace(System.err);
            session.setAttribute("registeredUser", "can not registered");
            resp.sendRedirect("registration.jsp");

        }
    }

}
