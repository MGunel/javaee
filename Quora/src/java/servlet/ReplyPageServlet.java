package servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pojos.Post;
import pojos.Reply;
import repository.ReplyRepository;

/**
 *
 * @author GUNEL
 */
@WebServlet("/page/reply")
public class ReplyPageServlet extends HttpServlet {

    private final ReplyRepository replyRepository;

    {
        replyRepository = new ReplyRepository();
    }
    HashMap<Long, Integer> nextmap;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(false);
        nextmap = (HashMap<Long, Integer>) session.getAttribute("replyCurrentPage");
        Long postId = Long.valueOf(req.getParameter("postId"));

        if (nextmap.get(postId) == null) {
            System.out.println("Olcu=1");
            System.out.println("POSTID==>" + postId);
            HashMap<Long, Integer> currentMap = (HashMap<Long, Integer>) session.getAttribute("replyCurrentPage");
            Integer currentVal = currentMap.get(1L);
            currentVal++;   //sonradan*
            List<Reply> nextPageData = replyRepository.findAllPaginatedBy(postId, 2, currentVal);
            List<Post> currentData = (List<Post>) session.getAttribute("posts");
            Post targetPost = currentData.stream().filter(p -> p.getId().equals(postId)).findFirst().get();
            targetPost.getReplies().addAll(nextPageData);
            session.setAttribute("posts", currentData);
            nextmap.put(postId, currentVal);
            session.setAttribute("replyCurrentPage", nextmap);
            System.out.println("replyCurrentPage==>***" + currentVal);
        } else {
            System.out.println("Else hal size boyukdir 1den**********");
            HashMap<Long, Integer> currentMap = (HashMap<Long, Integer>) session.getAttribute("replyCurrentPage");
            Integer currentVal = currentMap.get(postId);
            currentVal++;   //sonradan*
            List<Reply> nextPageData = replyRepository.findAllPaginatedBy(postId, 2, currentVal);
            List<Post> currentData = (List<Post>) session.getAttribute("posts");
            Post targetPost = currentData.stream().filter(p -> p.getId().equals(postId)).findFirst().get();
            targetPost.getReplies().addAll(nextPageData);
            session.setAttribute("posts", currentData);
            nextmap.put(postId, currentVal);
            session.setAttribute("replyCurrentPage", nextmap);
            System.out.println("replyCurrentPage==>***" + currentVal);

        }
        resp.sendRedirect("..//secured/home.jsp");
    }

}
