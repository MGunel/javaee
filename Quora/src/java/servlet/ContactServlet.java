/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pojos.Contact;
import pojos.User;
import repository.ContactRepository;

/**
 *
 * @author GUNEL
 */
@WebServlet("/contact")
public class ContactServlet extends HttpServlet {

    private final ContactRepository contactRepository;

    {
        contactRepository = new ContactRepository();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("I am Contact post");
        String name = (String) req.getParameter("name");
        String surname = (String) req.getParameter("surname");
        String email = (String) req.getParameter("email");
        String phone = (String) req.getParameter("phone");
        String message = (String) req.getParameter("message");
        HttpSession hs = req.getSession(true);
        User user = (User) hs.getAttribute("loginedUser");
        Long user_id = user.getId();
        System.out.println("User_id=" + user_id);
        HttpSession session=req.getSession();
        if ((message.equals("") || message.equals("null"))
                || (name.equals("") || name.equals("null"))
                || (surname.equals("") || surname.equals("null"))
                || (email.equals("") || email.equals("null"))
                || (phone.equals("") || phone.equals("null"))) {
            session.setAttribute("alertForContact", "All fields must fill...");
        } else {
            Contact contact = new Contact();
            contact.setFirstName(name);
            contact.setLastName(surname);
            contact.setEmail(email);
            contact.setPhone(phone);
            contact.setMessage(message);
            contact.setUser_id(user_id);
            contactRepository.save(contact);
        }
        resp.sendRedirect("contact.jsp");
    }

}
