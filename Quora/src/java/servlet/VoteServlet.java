package servlet;

import bussiness.PostServices;
import bussiness.PostServicesProvider;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pojos.Post;
import pojos.User;
import repository.PostRepository;
import repository.VoteRepository;

/**
 *
 * @author GUNEL
 */
@WebServlet("/vote")
public class VoteServlet extends HttpServlet {

    private final PostServices postServices;
    private final PostRepository postRepository;
    private final VoteRepository voteRepository;

    {
        postServices = new PostServicesProvider();
        postRepository = new PostRepository();
        voteRepository = new VoteRepository();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("VOTES POST");

        Long postId = Long.valueOf(req.getParameter("postid"));
        Post post = postRepository.findById(postId);
        User user = (User) req.getSession(false).getAttribute("loginedUser");
        boolean userLikedInPast = voteRepository.findVotesByPost(post.getId())
                .stream().filter(vote -> vote.getUser().getId().equals(user.getId()))
                .findAny().isPresent();
        if (userLikedInPast) {

            post = postServices.unVotePost(post, user);
            post.setLiked(false);
        } else {
            post = postServices.votePost(post, user);
            post.setLiked(true);
        }
        List<Post> currentPosts = (List<Post>) req.getSession(false).getAttribute("posts");
        System.out.println("Sortdan evvelki\n" + currentPosts);
        Comparator<Post> sortById = (Post o1, Post o2) -> Long.compare(o1.getId(), o2.getId());
        //int index = Collections.binarySearch(currentPosts, post, sortById);
        int index = currentPosts.indexOf(post);
        
        currentPosts.set(index, post);
        req.getSession(false).setAttribute("posts", currentPosts);
        resp.sendRedirect("secured/home.jsp");
    }

}
