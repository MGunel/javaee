package servlet;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pojos.Post;
import pojos.Reply;
import pojos.User;
import repository.PostRepository;
import repository.ReplyRepository;

/**
 *
 * @author GUNEL
 */
@WebServlet("/reply")
public class ReplyServlet extends HttpServlet {

    private final ReplyRepository replyRepository;
    private final PostRepository postRepository;

    {
        replyRepository = new ReplyRepository();
        postRepository = new PostRepository();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Reply's doGet working");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Reply's doPost working");
        String replyContent = req.getParameter("reply");
        Long targetPostId = Long.valueOf(req.getParameter("targetPost"));
        System.out.println("TargetPostId " + req.getParameter("targetPost"));
        Post targetPost = postRepository.findById(targetPostId);
        System.out.println(targetPost);
        User user = (User) req.getSession(false).getAttribute("loginedUser");
        System.out.println("ReplyContent " + replyContent);
        Reply reply = new Reply();
        reply.setAuthor(user);
        reply.setContent(replyContent);
        reply.setCreated(Timestamp.from(Instant.now()));
        reply.setPost(targetPost);
        replyRepository.save(reply);
        List<Post> posts = (List<Post>) req.getSession(false).getAttribute("posts");
        updatePostOnSession(posts, reply);
        req.getSession(false).setAttribute("posts", posts);
        resp.sendRedirect("secured/home.jsp");

    }

    public void updatePostOnSession(List<Post> posts, Reply... replies) {
        Reply targetReply = replies[0];
        //        for (Post post:posts) {
        //            if (post.getId().equals(targetReply.getPost().getId())) {
        //                
        //            }
        //        }
        Optional<Post> postOptional = posts.stream()
                .filter(post -> post.getId().equals(targetReply.getPost().getId()))
                .findFirst();
        if (postOptional.isPresent()) {
            List<Reply> repliesAsList = Arrays.asList(replies);
            if (postOptional.get().getReplies() != null) {
                postOptional.get().getReplies().addAll(repliesAsList);
                postOptional.get().setReplies(postOptional.get().getReplies());

            } else {
                postOptional.get().setReplies(repliesAsList);
            }
        }

    }

}
