/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import pojos.Post;
import pojos.User;
import repository.PostRepository;

/**
 *
 * @author GUNEL
 */
@WebServlet("/post")
public class PostServlet extends HttpServlet {
private final PostRepository repository;
{
    repository=new PostRepository();
}
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("/post cagrilir");
        User user=(User) req.getSession(false).getAttribute("loginedUser");
        String content =req.getParameter("content");
        Post newPost=createPost(content, "Not title", user);
        List<Post> currentPosts=(List<Post>) req.getSession(false).getAttribute("posts");
        Comparator<Post> orderByCreated=new Comparator<Post>() {
            @Override
            public int compare(Post post1, Post post2) {
                return post2.getCreated().compareTo(post1.getCreated());
            }
        };
        currentPosts.add(newPost);
        Collections.sort(currentPosts, orderByCreated);
        req.getSession(false).setAttribute("posts", currentPosts);
        resp.sendRedirect("secured/home.jsp");
    }

    public Post createPost(String content,String title,User user) {
        Post post=new Post();
        post.setContent(content);
        post.setAuthor(user);
        post.setTitle(title);
        //post.setCreated(new Date());
        post.setVote(0);
       return  repository.save(post);
    }
    
}
