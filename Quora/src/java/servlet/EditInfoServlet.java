package servlet;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Paths;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import pojos.Gender;
import pojos.PojoImage;
import pojos.User;
import repository.ImageRepository;
import repository.UserRepository;

/**
 *
 * @author GUNEL
 */
@WebServlet("/edit")
@MultipartConfig
public class EditInfoServlet extends HttpServlet {

    private final UserRepository userRepository;
    private final ImageRepository imageRepository;

    {
        userRepository = new UserRepository();
        imageRepository = new ImageRepository();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Hello Iam a post method");
        HttpSession session = req.getSession(true);
        User loginedUser = (User) session.getAttribute("loginedUser");
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String confirmpassword = req.getParameter("confirmpassword");
        String age = req.getParameter("age");
        String gender = req.getParameter("user[gender]");
        String user_profession = req.getParameter("user_profession");
        String user_country = req.getParameter("user_country");
        Long id = loginedUser.getId();
        //shekilin pathin ve adin gotururem
        HttpSession hs = req.getSession();
        String alertMessage = "";
        Part part = req.getPart("file");

        if (part.getSubmittedFileName().isEmpty() && confirmpassword.equals(password)) {

            System.out.println("Before update image==>" + loginedUser);
            User user = new User();
            user.setId(id);
            user.setName(name);
            user.setSurname(surname);
            user.setUsername(username);
            user.setPassword(password);
            user.setAge(Integer.parseInt(age));
            user.setCountry(user_country);
            user.setProfession(user_profession);
            user.setGender(Gender.valueOf(gender));
            userRepository.save(user);
            //imageRepository.save(pi);
            alertMessage = "Your information are changed,succesfully :)";

        } else if (!part.getSubmittedFileName().isEmpty() && confirmpassword.equals(password)) {

            String fileName = Paths.get(part.getSubmittedFileName()).getFileName().toString(); // MSIE fix.
            String path = "C:\\Users\\user\\Desktop\\resources"
                    + File.separator + fileName;
            part.write(path + File.separator);
            System.out.println("Before update image===" + loginedUser);
            User user = new User();
            user.setId(id);
            user.setName(name);
            user.setSurname(surname);
            user.setUsername(username);
            user.setPassword(password);
            user.setAge(Integer.parseInt(age));
            user.setCountry(user_country);
            user.setProfession(user_profession);
            user.setGender(Gender.valueOf(gender));
            PojoImage pi = new PojoImage();
            pi.setImgId(imageRepository.findByUser_id(user).getImgId());
            pi.setImgName(fileName);
            pi.setImgPath(path);
            pi.setUserId(id);
            user.setPojoImage(pi);
            //update
            userRepository.save(user);
            imageRepository.save(pi);
            session.setAttribute("loginedUser", user);
            alertMessage = "Your information are changed,succesfully :)";
        } else {
            if (!confirmpassword.equals(password)) {
                alertMessage = "Confirm Password and Password is not the same...";
            }
            if (part.getSubmittedFileName().isEmpty()) {

                alertMessage = alertMessage + " and file is not chosen";
            }
        }

        hs.setAttribute("alertMsg", alertMessage);

        req.getRequestDispatcher("profil.jsp").forward(req, resp);

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Hello Iam  Get Method");
        //resp.setContentType("image/xyz");
        ServletContext sc = getServletContext();

        User user = (User) req.getSession(true).getAttribute("loginedUser");

        String pathForImg = user.getPojoImage().getImgPath();
        File f = new File(pathForImg);
        String mimeType = sc.getMimeType(user.getPojoImage().getImgName());
        System.out.println("mimeType" + mimeType);
        // Set content type
        resp.setContentType(mimeType);
        BufferedImage bi = ImageIO.read(f);
        OutputStream out = resp.getOutputStream();
        ImageIO.write(bi, "jpg", out);
        out.close();

    }

}
