package servlet;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pojos.Post;
import pojos.User;
import pojos.Vote;
import repository.PostRepository;
import static repository.Paginable.DEFAULT_PAGE_SIZE;
import repository.VoteRepository;

/**
 *
 * @author GUNEL
 */
@WebServlet("/page/post")
public class PostPageServlet extends HttpServlet {

    private final PostRepository postRepository;
    private final VoteRepository voteRepository;//sonradan

    {
        postRepository = new PostRepository();
        voteRepository = new VoteRepository();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession currentSession = req.getSession(false);
        Integer currentPage = (Integer) currentSession.getAttribute("postCurrentPage");
        currentPage++;
        System.out.println("Current Page" + currentPage);
        List<Post> nextPageData = postRepository.findAllPaginated(DEFAULT_PAGE_SIZE, currentPage);
        System.out.println("Current Page" + currentPage);
        List<Post> currentData = (List<Post>) currentSession.getAttribute("posts");
        currentData.addAll(nextPageData);
        currentSession.setAttribute("posts", currentData);
        //sonradan
        User user=(User) currentSession.getAttribute("loginedUser");
        final long userId = user.getId();

        for (Post p : currentData) {
            List<Vote> votes = voteRepository.findVotesByPost(p.getId());
            p.setLiked(votes.stream().filter(vote -> vote.getUser().getId().equals(userId)).findAny().isPresent());

        }
        Comparator<Post> orderByCreated = new Comparator<Post>() {
            @Override
            public int compare(Post post1, Post post2) {
                return post2.getCreated().compareTo(post1.getCreated());
            }
        };
        Collections.sort(currentData, orderByCreated);
        //sonradan
        currentSession.setAttribute("postCurrentPage", currentPage);
        resp.sendRedirect("..//secured/home.jsp");
    }

}
