package repository;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import pojos.Post;
import pojos.Reply;

/**
 *
 * @author GUNEL
 */
public class ReplyRepository extends Connector implements GenericRepository<Reply>, Paginable<Reply> {

    private final UserRepository userRepository;

    public ReplyRepository() {
        userRepository = new UserRepository();
    }

    @Override
    public Reply findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Reply> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Reply> findAllByPost(Long post_id) {
        String query = "select * from replies where post_id=?";
        try {
            connect();
            statement = connection.prepareStatement(query);
            statement.setLong(1, post_id);
            result = statement.executeQuery();
            List<Reply> repliesByPost = new ArrayList<>();
            while (result.next()) {
                Reply reply = new Reply();
                reply.setId(result.getLong(1));
                reply.setContent(result.getString(2));
                reply.setAuthor(userRepository.findById(result.getLong(3)));
                reply.setCreated(result.getTimestamp(4));
                repliesByPost.add(reply);
            }
            return repliesByPost;
        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
        return null;
    }

    @Override
    public void remove(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Reply save(Reply t) {
        if (t.getId() == null) {
            String replyQuery = "insert into replies(content,author_id,created,post_id) values (?,?,?,?)";
            try {
                connect();
                statement = connection.prepareStatement(replyQuery, Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, t.getContent());
                statement.setLong(2, t.getAuthor().getId());
                statement.setTimestamp(3, (Timestamp) t.getCreated());
                statement.setLong(4, t.getPost().getId());
                statement.execute();
                result = statement.getGeneratedKeys();
                if (result.next()) {
                    Long id = result.getLong(1);
                    t.setId(id);
                }
                return t;
            } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
                e.printStackTrace(System.err);
            } finally {
                disConnect();
            }
        }
        return null;
    }

    @Override
    public List<Reply> findAllPaginated(int rowSize, int page) {
        List<Reply> paginatedReplies = new ArrayList<>();
        if (page > 0) {
            page--;
        }

        String query = "select * from replies r order by r.created asc limit ? offset ?";
        try {
            connect();
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setInt(1, rowSize);
            statement.setInt(2, page*rowSize);
            statement.setMaxRows(rowSize);
            result = statement.executeQuery();
            while (result.next()) {
                Reply reply = new Reply();
                reply.setId(result.getLong(1));
                reply.setContent(result.getString(2));
                reply.setAuthor(userRepository.findById(result.getLong(3)));
                reply.setCreated(result.getTimestamp(4));
                paginatedReplies.add(reply);
            }

        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
        return paginatedReplies;

    }

    @Override
    public List<Reply> findAllPaginatedBy(Long id, int rowSize, int page) {
        List<Reply> paginatedReplies = new ArrayList<>();
        if (page > 0) {
            page--;
        }

        String query = "select * from replies r where post_id=? order by r.created asc limit ? offset ?";
        try {
            connect();
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setLong(1, id);
            statement.setInt(2, rowSize);
            statement.setInt(3, page*rowSize);
            statement.setMaxRows(rowSize);
            result = statement.executeQuery();
            while (result.next()) {
                Reply reply = new Reply();
                reply.setId(result.getLong(1));
                reply.setContent(result.getString(2));
                reply.setAuthor(userRepository.findById(result.getLong(3)));
                reply.setCreated(result.getTimestamp(4));
                paginatedReplies.add(reply);
            }

        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
        return paginatedReplies;
    }

}
