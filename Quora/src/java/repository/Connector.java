package repository;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author GUNEL
 */
public class Connector {

    protected Connection connection;
    protected PreparedStatement statement;
    protected ResultSet result;
    private DataSource dataSourceMySql;

    public void connect() throws IOException, SQLException, ClassNotFoundException, NamingException {

//        Properties config = new Properties();
//            InputStream configFileStrim = Thread
//            .currentThread().getContextClassLoader().getResourceAsStream("/repository/db-config.properties");
//            config.load(configFileStrim);
//            Class.forName(config.getProperty("DRIVER_NAME"));
//            connection = DriverManager
//            .getConnection(config.getProperty("URL"),
//            config.getProperty("USERNAME"),
//            config.getProperty("PASSWORD"));
         
        
            Context ctx = new InitialContext();
            dataSourceMySql = (DataSource) ctx.lookup("java:comp/env/jdbc/quora");
            connection=dataSourceMySql.getConnection();
        
       
            
        

    }

    public void disConnect() {
        try {
            if (result != null) {
                result.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }

    }
}
