package repository;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import pojos.Gender;
import pojos.Post;
import pojos.User;
import pojos.Vote;

/**
 *
 * @author GUNEL
 */
public class VoteRepository extends Connector implements GenericRepository<Vote> {

    @Override
    public Vote findById(Long id) {
        String querry = "select * from votes v "
                + "join users u on v.user_id=u.id "
                + "join posts p on v.post_id=p.id where v.id=? ";
        try {
            connect();
            statement = connection.prepareStatement(querry);
            statement.setLong(1, id);
            result = statement.executeQuery();
            if (result.next()) {
                Vote vote = new Vote();
                vote.setId(result.getLong(1));
                //*vote.setVoted(result.getTime(4));
                vote.setVoted(result.getTimestamp(4));
                ////////////////////////USER/////////////////
                User user = new User();
                user.setId(result.getLong(5));
                user.setPassword(result.getString(6));
                user.setUsername(result.getString(7));
                user.setName(result.getString(8));
                user.setSurname(result.getString(9));
                user.setGender(Gender.valueOf(result.getString(10)));
                user.setAge(result.getInt(11));
                user.setProfession(result.getString(12));
                user.setCountry(result.getString(13));
                //*user.setRegistered(result.getDate(14));
                user.setRegistered(result.getTimestamp(14));
                /////////////////////////////////////////////
                ///////////////////////////POST//////////////
                Post post = new Post();
                post.setId(result.getLong(15));
                post.setTitle(result.getString(16));
                post.setContent(result.getString(17));
                post.setVote(result.getInt(18));
                post.setCreated(result.getTimestamp(19));
                ///////////////////////////////////////////
                vote.setUser(user);
                vote.setPost(post);
                return vote;
            }
        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
        return null;
    }

    @Override
    public List<Vote> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Vote> findVotesByPost(Long postId) {
        String query = "select * from votes v "
                + "join users u on v.user_id=u.id "
                + "join posts p on v.post_id=p.id where v.post_id=? ";
        List<Vote> votesByPost = new ArrayList<>();
        try {
            connect();
            statement = connection.prepareStatement(query);
            statement.setLong(1, postId);
            result = statement.executeQuery();
            while (result.next()) {
                Vote vote = new Vote();
                vote.setId(result.getLong(1));
                vote.setVoted(result.getTimestamp(4));
                ////////////////////////USER/////////////////
                User user = new User();
                user.setId(result.getLong(5));
                user.setPassword(result.getString(6));
                user.setUsername(result.getString(7));
                user.setName(result.getString(8));
                user.setSurname(result.getString(9));
                user.setGender(Gender.valueOf(result.getString(10)));
                user.setAge(result.getInt(11));
                user.setProfession(result.getString(12));
                user.setCountry(result.getString(13));
                user.setRegistered(result.getTimestamp(14));
                /////////////////////////////////////////////
                ///////////////////////////POST//////////////
                Post post = new Post();
                post.setId(result.getLong(15));
                post.setTitle(result.getString(16));
                post.setContent(result.getString(17));
                post.setVote(result.getInt(19));
                post.setCreated(result.getTimestamp(20));
                ///////////////////////////////////////////
                vote.setUser(user);
                vote.setPost(post);
                votesByPost.add(vote);
            }
        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
        return votesByPost;
    }

    @Override
    public void remove(Long id) {
        String removeQuery = "delete from votes where id=?";
        try {
            connect();
            statement = connection.prepareStatement(removeQuery);
            statement.setLong(1, id);
            statement.execute();
        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }

    }

    @Override
    public Vote save(Vote t) {
        String insertQuery = "INSERT INTO VOTES (USER_ID,POST_ID,CREATED)"
                + " VALUES(?,?,?)";
        try {
            connect();
            statement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, t.getUser().getId());
            statement.setLong(2, t.getPost().getId());
            statement.setTimestamp(3, Timestamp.from(Instant.now()));
            statement.execute();
            result = statement.getGeneratedKeys();
            if (result.next()) {
                Long id = result.getLong(1);
                t.setId(id);
            }
            return t;

        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
        return null;
    }

}
