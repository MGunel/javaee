/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.util.List;

/**
 *
 * @author GUNEL
 */
public interface GenericRepository<T> {
    T findById(Long id);
    List<T> findAll();
    void remove(Long id);
    T save(T t);
}
