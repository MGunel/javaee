package repository;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.naming.NamingException;
import pojos.PojoImage;
import pojos.User;

/**
 *
 * @author GUNEL
 */
public class ImageRepository extends Connector implements GenericRepository<PojoImage> {

    @Override
    public PojoImage findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<PojoImage> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public PojoImage save(PojoImage t) {
        String updateQuerry = "UPDATE images SET imgPath=?,imgName=? where user_id=?";
        String insertQuerry = "Insert into images (imgPath,imgName,user_id) values(?,?,?)";
        try {
            connect();
            
            if (t.getImgId() != null) {
                statement = connection.prepareStatement(updateQuerry);
                statement.setString(1, t.getImgPath());
                statement.setString(2, t.getImgName());
                statement.setLong(3, t.getUserId());
                statement.execute();
                return t;
            } else {
                statement = connection.prepareStatement(insertQuerry, Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, t.getImgPath());
                statement.setString(2, t.getImgName());
                statement.setLong(3, t.getUserId());
                statement.execute();
                result = statement.getGeneratedKeys();
                if (result.next()) {
                    Long id = result.getLong(1);
                    t.setImgId(id);
                }
                return t;
            }

        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
        return null;
    }

    public PojoImage findByUser_id(User user) {
        String query="SELECT * FROM images where user_id=?";
        try {
            connect();
            statement=connection.prepareStatement(query);
            statement.setLong(1, user.getId());
            result=statement.executeQuery();
            PojoImage pi=null;
            if (result.next()) {
              pi=new PojoImage();
              pi.setImgId(result.getLong(1));
              pi.setImgPath(result.getString(2));
              pi.setImgName(result.getString(3));
              pi.setUserId(user.getId());
            }
           return pi;
        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
        return null;
    }

}
