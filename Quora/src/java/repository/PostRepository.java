package repository;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import pojos.Gender;
import pojos.Post;
import pojos.User;

/**
 *
 * @author GUNEL
 */
public class PostRepository extends Connector implements GenericRepository<Post>, Paginable<Post> {

    private final ReplyRepository replyRepository;

    public PostRepository() {
        replyRepository = new ReplyRepository();
    }

    @Override
    public Post findById(Long id) {
        String query = "select * from posts p join users u"
                + " on(p.user_id=u.id) where p.id=? ";
        try {
            connect();
            statement = connection.prepareStatement(query);
            statement.setLong(1, id);
            result = statement.executeQuery();
            Post post = null;
            if (result.next()) {
                post = new Post();
                post.setId(result.getLong(1));
                post.setTitle(result.getString(2));
                post.setContent(result.getString(3));
                post.setVote(result.getInt(5));
                post.setCreated(result.getTimestamp(6));
                User user = new User();
                //////////////////////User///////////
                user.setId(result.getLong(7));
                user.setPassword(result.getString(8));
                user.setUsername(result.getString(9));
                user.setName(result.getString(10));
                user.setSurname(result.getString(11));
                user.setGender(Gender.valueOf(result.getString(12)));
                user.setAge(result.getInt(13));
                user.setProfession(result.getString(14));
                user.setCountry(result.getString(15));
                //*user.setRegistered(result.getDate(16));
                user.setRegistered(result.getTimestamp(16));

                ////////////////////////////////////
                post.setAuthor(user);
                return post;
            }

        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
            return null;
        } finally {
            disConnect();
        }
        return null;
    }

    @Override
    public List<Post> findAll() {
        String querry = "select * from posts p join users u"
                + " on(p.user_id=u.id)";
        List<Post> posts = new ArrayList<>();
        try {
            connect();
            statement = connection.prepareStatement(querry);
            result = statement.executeQuery();
            while (result.next()) {
                Post post = new Post();
                post.setId(result.getLong(1));
                post.setTitle(result.getString(2));
                post.setContent(result.getString(3));
                post.setVote(result.getInt(5));
                post.setCreated(result.getTimestamp(6));
                User user = new User();
                user.setId(result.getLong(7));
                user.setPassword(result.getString(8));
                user.setUsername(result.getString(9));
                user.setName(result.getString(10));
                user.setSurname(result.getString(11));
                user.setGender(Gender.valueOf(result.getString(12)));
                user.setAge(result.getInt(13));
                user.setProfession(result.getString(14));
                user.setCountry(result.getString(15));
                //*user.setRegistered(result.getDate(16));
                user.setRegistered(result.getTimestamp(16));
                post.setAuthor(user);
                post.setReplies(replyRepository.findAllByPost(post.getId()));
                posts.add(post);
            }
            return posts;
        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        }
        return null;
    }

    @Override
    public void remove(Long id) {
        String query = "delete from posts where id=? ";
        try {
            connect();
            statement = connection.prepareStatement(query);
            statement.setLong(1, id);
            statement.execute();
        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
    }

    @Override
    public Post save(Post t) {
        String saveQuery = "INSERT INTO POSTS (TITLE,CONTENT,USER_ID,VOTES,CREATED)"
                + "VALUES(?,?,?,?,?)";
        String updateQuery = "UPDATE POSTS SET TITLE=?,CONTENT=?,VOTES=? WHERE ID=?";
        try {
            connect();
            if (t.getId() != null) {
                statement = connection.prepareStatement(updateQuery);
                statement.setString(1, t.getTitle());
                statement.setString(2, t.getContent());
                statement.setInt(3, t.getVote());
                statement.setLong(4, t.getId());
                statement.execute();
                return t;
            } else {
                statement = connection.prepareStatement(saveQuery, Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, t.getTitle());
                statement.setString(2, t.getContent());
                statement.setLong(3, t.getAuthor().getId());
                statement.setInt(4, t.getVote());
//                System.out.println("Before date is" + t.getCreated());
//                java.sql.Date created = new java.sql.Date(t.getCreated().getTime());
//                statement.setDate(5, created);
//                System.out.println(created);
                t.setCreated(Timestamp.from(Instant.now()));
                statement.setTimestamp(5, (Timestamp) t.getCreated());
                statement.execute();
                result = statement.getGeneratedKeys();
                if (result.next()) {
                    Long id = result.getLong(1);
                    t.setId(id);
                }
                return t;
            }

        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            System.out.println(System.err);
        } finally {
            disConnect();
        }
        return null;
    }

    List<Post> getPostByUser(User user) {
        List<Post> myOwnPost = new ArrayList<>();
        try {
            connect();
            String query = "select * posts where user_id=?";
            statement = connection.prepareStatement(query);
            statement.setLong(1, user.getId());
            result = statement.executeQuery();
            while (result.next()) {
                Post post = new Post();
                post.setAuthor(user);
                post.setId(result.getLong(1));
                post.setTitle(result.getString(2));
                post.setContent(result.getString(3));
                post.setVote(result.getInt(5));
                //*post.setCreated(result.getTime(6));
                post.setCreated(result.getTimestamp(6));
                myOwnPost.add(post);
            }
        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
        return myOwnPost;
    }

    @Override
    public List<Post> findAllPaginated(int rowSize, int page) {
        List<Post> paginatedPosts = new ArrayList<>();
        if (page > 0) {
            page--;
        }
        System.out.println("Page Number in findAllPaginated" + page);
        String query = "SELECT *"
                + " FROM POSTS p "
                + "JOIN USERS u "
                + "ON (p.user_id=u.id) order by p.created desc "
                + "limit ? offset ? ";
        try {
            connect();
            statement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            statement.setInt(1, rowSize);
            statement.setInt(2, page * rowSize);
            statement.setMaxRows(rowSize);
            result = statement.executeQuery();
            while (result.next()) {
                Post post = new Post();
                post.setId(result.getLong(1));
                post.setTitle(result.getString(2));
                post.setContent(result.getString(3));
                post.setVote(result.getInt(5));
                post.setCreated(result.getTimestamp(6));
                User user = new User();
                user.setId(result.getLong(7));
                user.setPassword(result.getString(8));
                user.setUsername(result.getString(9));
                user.setName(result.getString(10));
                user.setSurname(result.getString(11));
                user.setGender(Gender.valueOf(result.getString(12)));
                user.setAge(result.getInt(13));
                user.setProfession(result.getString(14));
                user.setCountry(result.getString(15));
                //*user.setRegistered(result.getDate(16));
                user.setRegistered(result.getTimestamp(16));
                post.setAuthor(user);
                //post.setReplies(replyRepository.findAllByPost(post.getId()));
                post.setReplies(replyRepository.findAllPaginatedBy(post.getId(), 2, 0));
                paginatedPosts.add(post);
            }

        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
        return paginatedPosts;
    }

    @Override
    public List<Post> findAllPaginatedBy(Long id, int rowSize, int page) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
