package repository;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import javax.naming.NamingException;
import pojos.Gender;
import pojos.User;

/**
 *
 * @author GUNEL
 */
public class UserRepository extends Connector implements GenericRepository<User> {

    @Override
    public User findById(Long id) {
        try {
            connect();
            User user = null;
            String loginQuery = "SELECT * FROM USERS WHERE ID=?";
            statement = connection.prepareStatement(loginQuery);
            statement.setLong(1, id);
            result = statement.executeQuery();
            if (result.next()) {
                user = new User();
                user.setId(result.getLong(1));
                user.setPassword(result.getString(2));
                user.setUsername(result.getString(3));
                user.setName(result.getString(4));
                user.setSurname(result.getString(5));
                user.setGender(Gender.valueOf(result.getString(6)));
                user.setAge(result.getInt(7));
                user.setProfession(result.getString(8));
                user.setCountry(result.getString(9));
                //*user.setRegistered(result.getDate(10));
                user.setRegistered(result.getTimestamp(10));
            }
            return user;
        } catch (IOException | ClassNotFoundException | SecurityException | SQLException | NamingException e) {
            System.out.println(System.err);
        } finally {
            disConnect();
        }
        return null;
    }

    @Override
    public List<User> findAll() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void remove(Long id) {
        String removeQuerry = "delete from users where id =?";
        try {
            connect();
            statement = connection.prepareStatement(removeQuerry);
            statement.setLong(1, id);
            statement.execute();
        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
    }

    @Override
    public User save(User t) {
        String updateQuery = "UPDATE USERS SET PASSWORD=?, USERNAME=?,NAME=?,SURNAME=?,GENDER=?,AGE=?,PROFESSION=?,COUNTRY=?"
                + "WHERE ID=?";
        String insertQuery = "INSERT INTO USERS (PASSWORD,USERNAME,NAME,SURNAME,GENDER,AGE,PROFESSION,COUNTRY,REGISTERED)"
                + "VALUES(?,?,?,?,?,?,?,?,?)";
        try {
            connect();
            if (t.getId() != null) {
                statement = connection.prepareStatement(updateQuery);
                statement.setString(1, t.getPassword());
                statement.setString(2, t.getUsername());
                statement.setString(3, t.getName());
                statement.setString(4, t.getSurname());
                statement.setString(5, t.getGender().name());
                statement.setInt(6, t.getAge());
                statement.setString(7, t.getProfession());
                statement.setString(8, t.getCountry());
                statement.setLong(9, t.getId());
                statement.execute();
                return t;
            } else {
                statement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, t.getPassword());
                statement.setString(2, t.getUsername());
                statement.setString(3, t.getName());
                statement.setString(4, t.getSurname());
                statement.setString(5, t.getGender().name());
                statement.setInt(6, t.getAge());
                statement.setString(7, t.getProfession());
                statement.setString(8, t.getCountry());
                //*statement.setDate(9, new java.sql.Date(t.getRegistered().getTime()));
                statement.setTimestamp(9, (Timestamp) t.getRegistered());
                statement.execute();
                result = statement.getGeneratedKeys();
                if (result.next()) {
                    Long id = result.getLong(1);
                    t.setId(id);
                }
                return t;
            }

        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
        return null;
    }



    public User findByUsernameAndPassword(String username, String password) {
        String querry = "select * from users where username=? and password=?";
        User currentUser = null;
        try {
            connect();
            statement = connection.prepareStatement(querry);
            statement.setString(1, username);
            statement.setString(2, password);
            result = statement.executeQuery();
            if (result.next()) {
                currentUser = new User();
                currentUser.setId(result.getLong(1));
                currentUser.setPassword(result.getString(2));
                currentUser.setUsername(result.getString(3));
                currentUser.setName(result.getString(4));
                currentUser.setSurname(result.getString(5));
                currentUser.setGender(Gender.valueOf(result.getString(6)));
                currentUser.setAge(result.getInt(7));
                currentUser.setProfession(result.getString(8));
                currentUser.setCountry(result.getString(9));
                //*currentUser.setRegistered(result.getDate(10));
                currentUser.setRegistered(result.getTimestamp(10));
            }
        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            e.printStackTrace(System.err);
        } finally {
            disConnect();
        }
        return currentUser;
    }

}
