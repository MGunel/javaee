package repository;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import javax.naming.NamingException;
import pojos.Contact;

/**
 *
 * @author GUNEL
 */
public class ContactRepository  extends Connector implements GenericRepository<Contact>{

    @Override
    public Contact findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Contact> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void remove(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Contact save(Contact t) {
             String insertQuery="insert into contacts"
                     + " (firstName,lastName,email,phone,message,contactDate,user_id)"
                     + " values (?,?,?,?,?,?,?)";
             String updateQuery="";
             try {
            connect();
            if (t.getConId()!= null) {
                statement = connection.prepareStatement(updateQuery);
              
                statement.execute();
                return t;
            } else {
                statement = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, t.getFirstName());
                statement.setString(2, t.getLastName());
                statement.setString(3, t.getEmail());
                statement.setString(4, t.getPhone());
                statement.setString(5, t.getMessage());
                t.setContactDate(Timestamp.from(Instant.now()));
                statement.setTimestamp(6, (Timestamp) t.getContactDate());
                statement.setLong(7, t.getUser_id());
                statement.execute();
                result = statement.getGeneratedKeys();
                if (result.next()) {
                    Long conId = result.getLong(1);
                    t.setConId(conId);
                }
                return t;
            }

        } catch (IOException | ClassNotFoundException | SQLException | NamingException e) {
            System.out.println(System.err);
        } finally {
            disConnect();
        }
        return null;
        
    }
    
}
