/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.util.List;

/**
 *
 * @author GUNEL
 */
public interface Paginable<T> {
    int DEFAULT_PAGE_SIZE=5;
    List<T> findAllPaginated(int rowSize,int page);
    List<T> findAllPaginatedBy(Long id,int rowSize,int page);
}
