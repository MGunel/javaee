package bussiness;

import pojos.Post;
import pojos.Reply;
import pojos.User;

/**
 *
 * @author GUNEL
 */
public interface PostServices {
    Post votePost(Post post,User user);
    Post unVotePost(Post post,User user);
    void commentPost(Post post,Reply reply,User user);
}
