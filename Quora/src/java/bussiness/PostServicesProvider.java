package bussiness;

import java.util.List;
import java.util.Optional;
import pojos.Post;
import pojos.Reply;
import pojos.User;
import pojos.Vote;
import repository.PostRepository;
import repository.ReplyRepository;
import repository.VoteRepository;

/**
 *
 * @author GUNEL
 */
public class PostServicesProvider implements PostServices {
    
    private final PostRepository postRepository;
    private final VoteRepository voteRepository;
    private final ReplyRepository replyRepository;
    
    public PostServicesProvider() {
        postRepository = new PostRepository();
        voteRepository = new VoteRepository();
        replyRepository = new ReplyRepository();
    }
    
    @Override
    public Post votePost(Post post, User user) {
        int currentVote = post.getVote();
        currentVote++;
        post.setVote(currentVote);
        postRepository.save(post);
        Vote vote = new Vote();
        vote.setPost(post);
        vote.setUser(user);
        voteRepository.save(vote);
        return post;
    }
    
    @Override
    public Post unVotePost(Post post, User user) {
        List<Vote> currentVote = voteRepository.findVotesByPost(post.getId());
        Optional<Vote> optionalVote = currentVote.stream().filter(v -> v.getUser().equals(user)).findFirst();
        if (optionalVote.isPresent()) {
            Vote vote = optionalVote.get();
            voteRepository.remove(vote.getId());
            post.setVote(post.getVote() - 1);
            postRepository.save(post);
        }
        return post;
    }
    
    @Override
    public void commentPost(Post post, Reply reply, User user) {
        if (post != null && reply != null && user != null) {
          reply.setPost(post);
          reply.setAuthor(user);
          replyRepository.save(reply);
        }
        else
        {
            throw new IllegalArgumentException("arguments is null can not proceed");
        }
    }
    
}
