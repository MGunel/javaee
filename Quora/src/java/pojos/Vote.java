/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojos;

import java.util.Date;

/**
 *
 * @author GUNEL
 */
public class Vote {

    private Long id;
    private User user;
    private Post post;
    private Date voted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Date getVoted() {
        return voted;
    }

    public void setVoted(Date voted) {
        this.voted = voted;
    }

    @Override
    public String toString() {
        return "Vote{" + "id=" + id + ", user=" + user + ", post=" + post + ", voted=" + voted + '}';
    }
    
}
